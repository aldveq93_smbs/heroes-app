import React, { useContext } from 'react'
import { AuthContext } from '../../auth/AuthContext';
import MainLayout from '../../containers/MainLayout';
import { types } from '../../types/types';

const Login = ({history}) => {

    const { dispatch } = useContext(AuthContext);

    const handlerLogin = () => {

        const action = {
            type: types.login,
            payload: {
                name: 'Aldo'
            }
        }

        dispatch(action);

        history.replace('/');

    }

    return (
        <MainLayout>
            <h1>Login</h1>    
            <hr />
            <button 
                className="btn btn-primary"
                onClick={handlerLogin}
            >Login</button>
        </MainLayout>
    )
}

export default Login;