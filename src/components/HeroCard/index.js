import React from 'react'
import { Link } from 'react-router-dom'
import { heroImages } from '../../utilities/utilities';

const HeroCard = ({id, superhero, alter_ego, first_appearance}) => {

    return (
        <div className="col-12 col-md-3 mb-5">
            <div className="card">
                <img src={heroImages(`./${id}.jpg`).default} className="card-img-top" alt={`${superhero}`} />
                <div className="card-body">
                    <h5 className="card-title">{superhero}</h5>
                    <p className="card-text">{alter_ego}</p>
                    <p className="card-text"><small className="text-muted">{first_appearance}</small></p>
                </div>
                <div className="card-footer">
                    <Link to={`/heroes/${id}`} className="btn btn-primary">See hero</Link>
                </div>
            </div>
        </div>
    )
}

export default HeroCard;