import React from 'react'
import MainLayout from '../../containers/MainLayout';
import HeroList from '../HeroList';

const Marvel = () => {
    return (
        <MainLayout>
            <h1>Marvel Heroes</h1>
            <hr />
            <HeroList publisher='Marvel Comics' />
        </MainLayout>
    )
}

export default Marvel;