import React from 'react';
import { useHeroList } from '../../hooks/useHeroList';
import HeroCard from '../HeroCard';

const HeroList = ({publisher}) => {
    
    const heroesList= useHeroList(publisher);
    const heroesToRender = heroesList.map(hero => <HeroCard key={hero.id} {...hero} />);
    
    return (
        <div className="row card-group">
            {heroesToRender}
        </div>
    )
}

export default HeroList;