import React, { useMemo } from 'react';
import MainLayout from '../../containers/MainLayout';
import { useForm } from '../../hooks/useForm';
import HeroCard from '../HeroCard';
import queryString from 'query-string';
import { useLocation } from 'react-router-dom';
import { HeroService } from '../../service';
import Feedback from '../Feedback';

const Search = ({history}) => {

    const heroService = useMemo(() => new HeroService(), []);
    const location = useLocation();
    const { q = '' } = queryString.parse(location.search);
    const { heroText, onHandleHeroesSearch, resetHeroText } = useForm(q);

    const heroesFromService = useMemo(() => heroService.getHeroesByName(q), [heroService, q]);
    
    const onHandleSubmit = e => {
        e.preventDefault();
        
        if(heroText === '') {
            alert('Please, provide a search term!');
            return;
        }
        
        history.push(`?q=${heroText}`);
        resetHeroText();
    }

    const drawHeroes = () => {

        if(q === '') 
            return <Feedback type='warning' heading='Hey!' description='Try searching a hero!' />

        if(heroesFromService.length <= 0) 
            return <Feedback type='danger' heading='Oh, snap!' description={`We didn't find results related to your "${q}" search term`} />  
            

        return heroesFromService.map(hero => <HeroCard key={hero.id} {...hero} />);
        
    }

    return (
        <MainLayout>
            <div className="row">
                <div className="col-12">
                    <form className="row g-3" onSubmit={onHandleSubmit}>
                        <div className="col-auto">
                            <label htmlFor="inputPassword2" className="visually-hidden">Password</label>
                            <input type="text" className="form-control" placeholder="Search term" value={heroText} onChange={onHandleHeroesSearch} />
                        </div>
                        <div className="col-auto">
                            <button type="submit" className="btn btn-primary mb-3">Search</button>
                        </div>
                    </form>
                </div>
                <div className="col-12">
                    <div className="row">
                        {drawHeroes()}
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Search;