import React from 'react'
import MainLayout from '../../containers/MainLayout';
import HeroList from '../HeroList';

const DC = () => {
    return (
        <MainLayout>
            <h1>DC Heroes</h1>
            <hr />
            <HeroList publisher='DC Comics' />
        </MainLayout>
    )
}

export default DC;