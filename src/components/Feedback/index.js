import React from 'react'

const Feedback = ({type, heading, description}) => {
    return (
        <div className="col-12 mt-5">
            <div className={`alert alert-${type}`} role="alert">
                <h4 className="alert-heading">{heading}</h4>
                <p>{description}</p>
            </div>
        </div>
    )
}

export default Feedback;