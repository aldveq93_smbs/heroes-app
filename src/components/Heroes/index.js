import React from 'react'
import { Redirect, useParams } from 'react-router-dom';
import { useHeroData } from '../../hooks/useHeroData';
import MainLayout from '../../containers/MainLayout';
import { heroImages } from '../../utilities/utilities';

const Heroes = ({history}) => {
    const { id } = useParams();
    const heroData = useHeroData(id);

    if(!heroData)
        return <Redirect to='/' />
    
    const {
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters
    } = heroData;
    
    const goBack = () => {
        if(history.length <= 2) {
            history.push('/');
        } else {
            history.goBack();
        }
    }
     
    return (
        <MainLayout>
            <h1>Hero Specs</h1>
            <hr />
            <div className="row mt-5">
                <div className="col-4">
                    <img 
                        src={heroImages(`./${id}.jpg`).default}
                        alt={superhero}
                        className="img-thumbnail" 
                    />
                </div>
                <div className="col-8">
                    <h3 className="mb-4">{superhero}</h3>
                    <ul className="list-group list-group-flush mb-3">
                        <li className="list-group-item"><b>Alter ego: </b> {alter_ego}</li>
                        <li className="list-group-item"><b>Publisher: </b> {publisher}</li>
                        <li className="list-group-item"><b>First Appearance: </b> {first_appearance}</li>
                    </ul>

                    <h3>Characters</h3>
                    <p className="mb-5">{characters}</p>

                    <button 
                        className="btn btn-outline-primary"
                        onClick={goBack}
                    >Go back</button>
                </div>
            </div>
        </MainLayout>
    )
}

export default Heroes;