import { heroes } from "../data/heroes";

class HeroService {

    getHeroesByPublisher(publisher) {
        const publisherShouldBe = ['DC Comics', 'Marvel Comics'];

        if(!publisherShouldBe.includes(publisher))
            throw new Error(`The publisher is not correct! The possible ones are ${publisherShouldBe[0]} and ${publisherShouldBe[1]}`);

        return heroes.filter(hero => hero.publisher === publisher);
    }

    getHeroById(id) {
        return heroes.find(hero => hero.id === id);
    }

    getHeroesByName(name) {
        return heroes.filter(hero => ( ((hero.superhero).trim()).toLowerCase()).indexOf(((name.trim()).toLowerCase())) !== -1 );
    }
}

export {HeroService};