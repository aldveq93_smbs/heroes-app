import { useCallback, useMemo } from "react";
import { HeroService } from "../service";

const useHeroData = id => {

    const getHeroDataCallback = useCallback(() => {
        const heroService = new HeroService();
        const getHeroData = heroService.getHeroById(id);
        return getHeroData;
    }, [id]);

    const heroData = useMemo(() => getHeroDataCallback, [getHeroDataCallback]);

    return heroData();
}

export {useHeroData};