import { useCallback, useMemo } from "react";
import { HeroService } from "../service";

const useHeroList = publisher => {

    const getHeroes = useCallback(() => {
        const heroService = new HeroService();
        const heroesByPublisher = heroService.getHeroesByPublisher(publisher);
        return heroesByPublisher;
    }, [publisher]);

    const heroesList = useMemo(() => getHeroes, [getHeroes]);   

    return heroesList();
}

export {useHeroList};