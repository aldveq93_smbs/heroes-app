import { useState } from "react";

const useForm = (initialState = '') => {
    const [heroText, setHeroText] = useState(initialState);

    const onHandleHeroesSearch = ({target}) => {
        setHeroText(target.value);
    }

    const resetHeroText = () => {
        setHeroText('');
    }
    
    return {
        heroText,
        onHandleHeroesSearch,
        resetHeroText
    }
}

export {useForm};