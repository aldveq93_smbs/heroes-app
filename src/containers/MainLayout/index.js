import React from 'react'

const MainLayout = ({children}) => {
    return (
        <div className="container pt-5">
            {children}
        </div>
    )
}

export default MainLayout;