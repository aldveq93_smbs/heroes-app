import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router';

const PrivateRoute = ({
    isLoggedIn,
    component: Component,
    ...rest
}) => {

    const handlerConditionalRoute = (props) => {
        if(isLoggedIn)
            return <Component {...props} />;
        
        return <Redirect to='/login' />;
    }

    return (
        <Route 
            {...rest}
            component={handlerConditionalRoute}
        />
    )
}

export default PrivateRoute;

PrivateRoute.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}