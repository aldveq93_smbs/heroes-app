import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'
import DC from '../components/DC'
import Heroes from '../components/Heroes'
import Marvel from '../components/Marvel'
import Search from '../components/Search'
import { Navbar } from '../components/UI/Navbar'

export const DashboardRoutes = () => {
    return (
        <>
            <Navbar/>
            <Switch>
                <Route exact path='/marvel' component={Marvel} />
                <Route exact path='/heroes/:id' component={Heroes} />
                <Route exact path='/dc' component={DC} />
                <Route exact path='/search' component={Search} />
                <Redirect to='/marvel' />
            </Switch>
        </>
    )
}
