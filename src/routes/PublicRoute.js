import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, Route } from 'react-router';

const PublicRoute = ({
    isLoggedIn,
    component: Component,
    ...rest
}) => {

    const handlerConditionalRoute = (props) => {
        if(!isLoggedIn)
            return <Component {...props} />;
        
        return <Redirect to='/' />;
    }

    return (
        <Route 
            {...rest}
            component={handlerConditionalRoute}
        />
    )
}

export default PublicRoute;

PublicRoute.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
    component: PropTypes.func.isRequired
}